/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('Chart.Application', {
    extend: 'Ext.app.Application',
    name: 'Chart',
    stores: [
        // TODO: add global / shared stores here
    ],
    launch: function () {
        this.getConfig();
    },
    getConfig: function () {
        
        var that = this;
        Ext.Ajax.request({
            url: '../getMenu',
            success: function (response) {
                var config = Ext.JSON.decode(response.responseText);
                Ext.define('Chart.Config', {
                    singleton: true
                });
                Ext.apply(Chart.Config, config);
                
                that.fireEvent('configLoaded', that);
            },
            failure: function (response) {
                console.error(response);
            }
        });
    }
});
