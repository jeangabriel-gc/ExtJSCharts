Ext.define('Chart.view.charts.BarChartController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.charts-barchart',
    
        
    init: function() {
        var that = this;
        
        Ext.Ajax.request({
            url: '../getChartConfig',
            success: function (response) {
                var bob = eval(response.responseText);
                that.getView().add(bob);
                
            },
            failure: function (response) {
                console.error(response);
            }
        });
    }
    
});
