
Ext.define("Chart.view.charts.3DBarChart", {
    extend: "Ext.panel.Panel",
    requires: [
        'Chart.view.charts.3DBarChartController',
        'Chart.view.charts.3DBarChartModel',
        'Ext.chart.theme.Muted',
        'Ext.chart.axis.Numeric3D',
        'Ext.chart.axis.sprite.Axis3D',
        'Ext.chart.grid.HorizontalGrid3D',
        'Ext.chart.axis.Category3D',
        'Ext.chart.grid.VerticalGrid3D',
        'Ext.chart.series.Bar3D'
    ],
    controller: "charts-3dbarchart",
    viewModel: {
        type: "charts-3dbarchart"
    },
    items: [{
            xtype: 'cartesian',
            width: 800,
            height: 600,
            store: {
                fields: ['quarter', '2013', '2014'],
                data: [
                    {"quarter": "Q1", "2013": 42000, "2014": 68000},
                    {"quarter": "Q2", "2013": 50000, "2014": 85000},
                    {"quarter": "Q3", "2013": 53000, "2014": 72000},
                    {"quarter": "Q4", "2013": 63000, "2014": 89000}
                ]
            },
            theme: 'Muted',
            insetPadding: '70 40 0 40',
            interactions: ['itemhighlight'],
            animation: {
                duration: 200
            },
            legend: {
                docked: 'right'
            },
            sprites: [{
                    type: 'text',
                    text: 'Sales in Last Two Years',
                    textAlign: 'center',
                    fontSize: 18,
                    fontWeight: 'bold',
                    width: 100,
                    height: 30,
                    x: 325, // the sprite x position
                    y: 30  // the sprite y position
                }, {
                    type: 'text',
                    text: 'Quarter-wise comparison',
                    textAlign: 'center',
                    fontSize: 16,
                    x: 325,
                    y: 50
                }, {
                    type: 'text',
                    text: 'Source: http://www.w3schools.com/',
                    fontSize: 10,
                    x: 12,
                    y: 495
                }],
            axes: [{
                    type: 'numeric3d',
                    position: 'left',
                    fields: ['2013', '2014'],
                    grid: true,
                    title: 'Sales in USD',
                    renderer: function (v, layoutContext) {
                        var value = layoutContext.renderer(v) / 1000;
                        return value === 0 ? '$0' : Ext.util.Format.number(value, '$0K');
                    }
                }, {
                    type: 'category3d',
                    position: 'bottom',
                    fields: 'quarter',
                    title: {
                        text: 'Quarter',
                        translationX: -30
                    },
                    grid: true
                }],
            series: {
                type: 'bar3d',
                stacked: false,
                title: ['Previous Year', 'Current Year'],
                xField: 'quarter',
                yField: ['2013', '2014'],
                label: {
                    field: ['2013', '2014'],
                    display: 'insideEnd',
                    renderer: function (value) {
                        return Ext.util.Format.number(value / 1000, '$0K');
                    }
                },
                highlight: true,
                style: {
                    inGroupGapWidth: -7
                }
            }
        }]
});
