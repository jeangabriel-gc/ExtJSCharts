
Ext.define("Chart.view.charts.PieChart", {
    extend: "Ext.panel.Panel",
    requires: [
        'Chart.view.charts.PieChartController',
        'Chart.view.charts.PieChartModel',
        'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate',
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.theme.Category5Gradients'
    ],
    controller: "charts-piechart",
    viewModel: {
        type: "charts-piechart"
    },
    items: [{
            xtype: 'polar',
            width: 800,
            height: 600,
            store: {
                fields: ['os', 'marketShare'],
                data: [
                    {os: 'Android', marketShare: 68.3},
                    {os: 'BlackBerry', marketShare: 1.7},
                    {os: 'iOS', marketShare: 17.9},
                    {os: 'Windows Phone', marketShare: 10.2},
                    {os: 'Others', marketShare: 1.9}
                ]
            },
            theme: 'category5-gradients',
            animate: true,
            shadow: true,
            legend: {
                docked: 'bottom'
            },
            insetPadding: 40,
            innerPadding: 20,
            interactions: ['rotate', 'itemhighlight'],
            sprites: [{
                    type: 'text',
                    text: 'Pie Charts - Basic',
                    fontSize: 22,
                    width: 100,
                    height: 30,
                    x: 40, // the sprite x position
                    y: 20  // the sprite y position
                }, {
                    type: 'text',
                    text: 'Data: IDC Predictions - 2017',
                    x: 12,
                    y: 425
                }, {
                    type: 'text',
                    text: 'Source: Internet',
                    x: 12,
                    y: 440
                }],
            series: [{
                    type: 'pie',
                    angleField: 'marketShare',
                    label: {
                        field: 'os',
                        calloutLine: {
                            length: 60,
                            width: 3
                                    // specifying 'color' is also possible here
                        }
                    },
                    highlight: true,
                    tooltip: {
                        trackMouse: true,
                        renderer: function (storeItem, item) {
                            this.setHtml(storeItem.get('os') + ': ' + storeItem.get('marketShare') + '%');
                        }
                    }
                }]
        }]
});

///**
// *  A basic pie chart is a circular chart divided into multiple sectors in proportion to
// *  the data they represent. They are widely used and are helpful in quickly identifying
// *  smallest and largest segments of the data.
// *
// *  The example makes use of two interactions: 'itemhighlight' and 'rotate'. To use the
// *  first one, hover over or tap on a pie sector. To use the second one, click or tap and
// *  then drag anywhere on the chart.
// */
//Ext.define('Chart.view.charts.PieChart', {
//    extend: 'Ext.Panel',
//    requires: [
//        'Ext.chart.PolarChart',
//        'Ext.chart.series.Pie',
//        'Ext.chart.interactions.Rotate',
//        'Ext.chart.interactions.ItemHighlight',
//        'Ext.chart.theme.DefaultGradients'
//    ],
//    xtype: 'pie-basic',
//
//
//    width: 650,
//
//    initComponent: function() {
//        var me = this;
//
//        me.myDataStore = Ext.create('Ext.data.JsonStore', {
//            fields: ['os', 'data1' ],
//            data: [
//                { os: 'Android', data1: 68.3 },
//                { os: 'BlackBerry', data1: 1.7 },
//                { os: 'iOS', data1: 17.9 },
//                { os: 'Windows Phone', data1: 10.2 },
//                { os: 'Others', data1: 1.9 }
//            ]
//        });
//
//
//        me.items = [{
//            xtype: 'polar',
//            theme: 'default-gradients',
//            width: '100%',
//            height: 500,
//            store: me.myDataStore,
//            insetPadding: 50,
//            innerPadding: 20,
//            legend: {
//                docked: 'bottom'
//            },
//            interactions: ['rotate', 'itemhighlight'],
//            sprites: [{
//                type: 'text',
//                text: 'Pie Charts - Basic',
//                fontSize: 22,
//                width: 100,
//                height: 30,
//                x: 40, // the sprite x position
//                y: 20  // the sprite y position
//            }, {
//                type: 'text',
//                text: 'Data: IDC Predictions - 2017',
//                x: 12,
//                y: 425
//            }, {
//                type: 'text',
//                text: 'Source: Internet',
//                x: 12,
//                y: 440
//            }],
//            series: [{
//                type: 'pie',
//                angleField: 'data1',
//                label: {
//                    field: 'os',
//                    calloutLine: {
//                        length: 60,
//                        width: 3
//                        // specifying 'color' is also possible here
//                    }
//                },
//                highlight: true,
//                tooltip: {
//                    trackMouse: true,
//                    renderer: function(storeItem, item) {
//                        this.setHtml(storeItem.get('os') + ': ' + storeItem.get('data1') + '%');
//                    }
//                }
//            }]
//        }];
//
//        this.callParent();
//    }
//});
