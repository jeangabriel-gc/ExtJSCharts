
Ext.define("Chart.view.charts.BarChart", {
    extend: "Ext.panel.Panel",
    xtype: 'column-basic',
    requires: [
        'Chart.view.charts.BarChartController',
        'Chart.view.charts.BarChartModel',
        'Ext.chart.CartesianChart',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.series.Bar',
        'Ext.chart.interactions.ItemHighlight'
    ],
    controller: "charts-barchart",
    viewModel: {
        type: "charts-barchart"
    }
//    items: [
//        {
//            xtype: 'cartesian',
//            width: 800,
//            height: 600,
//            insetPadding: {
//                top: 40,
//                bottom: 40,
//                left: 20,
//                right: 40
//            },
//            interactions: 'itemhighlight',
//            store: {
//                fields: ['month', 'highF'],
//                data: [
//                    {"month": "Jan", "highF": 58.5},
//                    {"month": "Feb", "highF": 61.7},
//                    {"month": "Mar", "highF": 65.5},
//                    {"month": "Apr", "highF": 69.4},
//                    {"month": "May", "highF": 73.9},
//                    {"month": "Jun", "highF": 79.2},
//                    {"month": "Jul", "highF": 81.9},
//                    {"month": "Aug", "highF": 81.7},
//                    {"month": "Sep", "highF": 79.5},
//                    {"month": "Oct", "highF": 74.5},
//                    {"month": "Nov", "highF": 62.9},
//                    {"month": "Dec", "highF": 58.5}
//                ]
//            },
//            //define the x and y-axis configuration.
//            axes: [{
//                    type: 'numeric',
//                    position: 'left',
//                    minimum: 40,
//                    titleMargin: 20,
//                    title: {
//                        text: 'Temperature in °F'
//                    },
//                    listeners: {
//                        rangechange: function (axis, range) {
//                            var store = this.getChart().getStore(),
//                                    min = Infinity,
//                                    max = -Infinity,
//                                    value;
//
//                            store.each(function (rec) {
//                                var value = rec.get('highF');
//                                if (value > max) {
//                                    max = value;
//                                }
//                                if (value < min) {
//                                    min = value;
//                                }
//                            });
//
//                            value = (min + max) / 2;
//                            this.setLimits({
//                                value: value,
//                                line: {
//                                    title: {
//                                        text: 'Average high: ' + value.toFixed(2) + '°F'
//                                    },
//                                    lineDash: [2, 2]
//                                }
//                            });
//                        }
//                    }
//                }, {
//                    type: 'category',
//                    position: 'bottom'
//                }],
//            //define the actual bar series.
//            series: {
//                type: 'bar',
//                xField: 'month',
//                yField: 'highF',
//                colors: ['#456789'],
//                style: {
//                    minGapWidth: 15
//                },
//                highlight: {
//                    strokeStyle: 'black',
//                    fillStyle: 'gold',
//                    lineDash: [5, 3]
//                },
//                label: {
//                    field: 'highF',
//                    display: 'insideEnd',
//                    renderer: function (value) {
//                        return value.toFixed(1);
//                    }
//                }
//            },
//            sprites: {
//                type: 'text',
//                text: 'Redwood City Climate Data',
//                fontSize: 22,
//                width: 100,
//                height: 30,
//                x: 40, // the sprite x position
//                y: 20  // the sprite y position
//            }
//        }]
});

///**
// * The Basic Column Chart displays a set of random data in a column series. The "Reload Data"
// * button will randomly generate a new set of data in the store.
// *
// * Tapping or hovering a column will highlight it.
// */
//Ext.define('KitchenSink.view.charts.column.Basic', {
//    extend: 'Ext.Panel',
//    xtype: 'column-basic',
//
//
//    width: 650,
//    height: 500,
//
//    initComponent: function () {
//        var me = this;
//
//        me.items = {
//            xtype: 'cartesian',
//            store: {
//                type: 'climate'
//            },
//            insetPadding: {
//                top: 40,
//                bottom: 40,
//                left: 20,
//                right: 40
//            },
//            interactions: 'itemhighlight',
//            axes: [{
//                type: 'numeric',
//                position: 'left',
//                minimum: 40,
//                titleMargin: 20,
//                title: {
//                    text: 'Temperature in °F'
//                },
//                listeners: {
//                    rangechange: function (axis, range) {
//                        var store = this.getChart().getStore(),
//                            min = Infinity,
//                            max = -Infinity,
//                            value;
//
//                        store.each(function (rec) {
//                            var value = rec.get('highF');
//                            if (value > max) {
//                                max = value;
//                            }
//                            if (value < min) {
//                                min = value;
//                            }
//                        });
//
//                        value = (min + max) / 2;
//                        this.setLimits({
//                            value: value,
//                            line: {
//                                title: {
//                                    text: 'Average high: ' + value.toFixed(2) + '°F'
//                                },
//                                lineDash: [2,2]
//                            }
//                        });
//                    }
//                }
//            }, {
//                type: 'category',
//                position: 'bottom'
//            }],
//            animation: Ext.isIE8 ? false : {
//                easing: 'backOut',
//                duration: 500
//            },
//            series: {
//                type: 'bar',
//                xField: 'month',
//                yField: 'highF',
//                style: {
//                    minGapWidth: 20
//                },
//                highlight: {
//                    strokeStyle: 'black',
//                    fillStyle: 'gold',
//                    lineDash: [5, 3]
//                },
//                label: {
//                    field: 'highF',
//                    display: 'insideEnd',
//                    renderer: function (value) {
//                        return value.toFixed(1);
//                    }
//                }
//            },
//            sprites: {
//                type: 'text',
//                text: 'Redwood City Climate Data',
//                fontSize: 22,
//                width: 100,
//                height: 30,
//                x: 40, // the sprite x position
//                y: 20  // the sprite y position
//            }
//        };
//
//        this.callParent();
//    }
//});
