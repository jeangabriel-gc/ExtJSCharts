
Ext.define("Chart.view.charts.MultiLineChart", {
    extend: "Ext.panel.Panel",
    requires: [
        'Chart.view.charts.MultiLineChartController',
        'Chart.view.charts.MultiLineChartModel',
        'Ext.chart.series.Line'
    ],
    controller: "charts-multilinechart",
    viewModel: {
        type: "charts-multilinechart"
    },
    items: [{
            xtype: 'cartesian',
            width: 800,
            height: 600,
            legend: {
                docked: 'right'
            },
            store: {
                fields: ['month', 'ie', 'ff', 'ch', "sa"],
                data: [
                    {"month": "Jan", "ie": 20, "ff": 37, "ch": 35, "sa": 4},
                    {"month": "Feb", "ie": 20, "ff": 37, "ch": 36, "sa": 5},
                    {"month": "Mar", "ie": 19, "ff": 36, "ch": 37, "sa": 4},
                    {"month": "Apr", "ie": 18, "ff": 36, "ch": 38, "sa": 5},
                    {"month": "May", "ie": 18, "ff": 35, "ch": 39, "sa": 4},
                    {"month": "Jun", "ie": 17, "ff": 34, "ch": 42, "sa": 4},
                    {"month": "Jul", "ie": 16, "ff": 34, "ch": 43, "sa": 4},
                    {"month": "Aug", "ie": 16, "ff": 33, "ch": 44, "sa": 4},
                    {"month": "Sep", "ie": 16, "ff": 32, "ch": 44, "sa": 4},
                    {"month": "Oct", "ie": 16, "ff": 32, "ch": 45, "sa": 4},
                    {"month": "Nov", "ie": 15, "ff": 31, "ch": 46, "sa": 4},
                    {"month": "Dec", "ie": 15, "ff": 31, "ch": 47, "sa": 4}
                ]
            },
            insetPadding: 40,
            sprites: [{
                    type: 'text',
                    text: 'Line Charts - Marked Lines',
                    fontSize: 22,
                    width: 100,
                    height: 30,
                    x: 40, // the sprite x position
                    y: 20  // the sprite y position
                }, {
                    type: 'text',
                    text: 'Data: Browser Stats 2012',
                    fontSize: 10,
                    x: 12,
                    y: 470
                }, {
                    type: 'text',
                    text: 'Source: http://www.w3schools.com/',
                    fontSize: 10,
                    x: 12,
                    y: 485
                }],
            axes: [{
                    type: 'numeric',
                    fields: ['ie', 'ff', 'ch', 'sa'],
                    position: 'left',
                    grid: true,
                    minimum: 0,
                    renderer: function (v) {
                        return v.toFixed(v < 10 ? 1 : 0) + '%';
                    }
                }, {
                    type: 'category',
                    fields: 'month',
                    position: 'bottom',
                    grid: true,
                    label: {
                        rotate: {
                            degrees: -45
                        }
                    }
                }],
            series: [{
                    type: 'line',
                    axis: 'left',
                    title: 'IE',
                    xField: 'month',
                    yField: 'ie',
                    marker: {
                        type: 'square',
                        fx: {
                            duration: 200,
                            easing: 'backOut'
                        }
                    },
                    highlightCfg: {
                        scaling: 2
                    },
                    tooltip: {
                        trackMouse: true,
                        style: 'background: #fff',
                        renderer: function (storeItem, item) {
                            var title = item.series.getTitle();
                            this.setHtml(title + ' for ' + storeItem.get('month') + ': ' + storeItem.get(item.series.getYField()) + '%');
                        }
                    }
                }, {
                    type: 'line',
                    axis: 'left',
                    title: 'Firefox',
                    xField: 'month',
                    yField: 'ff',
                    marker: {
                        type: 'triangle',
                        fx: {
                            duration: 200,
                            easing: 'backOut'
                        }
                    },
                    highlightCfg: {
                        scaling: 2
                    },
                    tooltip: {
                        trackMouse: true,
                        style: 'background: #fff',
                        renderer: function (storeItem, item) {
                            var title = item.series.getTitle();
                            this.setHtml(title + ' for ' + storeItem.get('month') + ': ' + storeItem.get(item.series.getYField()) + '%');
                        }
                    }
                }, {
                    type: 'line',
                    axis: 'left',
                    title: 'Chrome',
                    xField: 'month',
                    yField: 'ch',
                    marker: {
                        type: 'arrow',
                        fx: {
                            duration: 200,
                            easing: 'backOut'
                        }
                    },
                    highlightCfg: {
                        scaling: 2
                    },
                    tooltip: {
                        trackMouse: true,
                        style: 'background: #fff',
                        renderer: function (storeItem, item) {
                            var title = item.series.getTitle();
                            this.setHtml(title + ' for ' + storeItem.get('month') + ': ' + storeItem.get(item.series.getYField()) + '%');
                        }
                    }
                }, {
                    type: 'line',
                    axis: 'left',
                    title: 'Safari',
                    xField: 'month',
                    yField: 'sa',
                    marker: {
                        type: 'cross',
                        fx: {
                            duration: 200,
                            easing: 'backOut'
                        }
                    },
                    highlightCfg: {
                        scaling: 2
                    },
                    tooltip: {
                        trackMouse: true,
                        style: 'background: #fff',
                        renderer: function (storeItem, item) {
                            var title = item.series.getTitle();
                            this.setHtml(title + ' for ' + storeItem.get('month') + ': ' + storeItem.get(item.series.getYField()) + '%');
                        }
                    }
                }]
        }]
});
