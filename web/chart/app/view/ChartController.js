Ext.define('Chart.view.ChartController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart',
    listen: {
        controller: {
            '*': {
                selectionChanged: "onMenuSelect"
            }
        }
    },
    onMenuSelect: function (chartType, chartName) {
        this.clearChart();
        this.loadChart(chartType, chartName);
    },
    loadChart: function (chartType, chartName) {

        var that = this;

        var url = "";
        var params = "";
        if (chartType === "sql" || chartType === "other") {
            url = "../getChartTemplate";
            params = {
                chartName: chartName,
                chartType: chartType
            }
        }
        else {
            url = "../getChartTemplateWithData";
            params = {
                chartName: chartName
            }
        }
        Ext.Ajax.request({
            url: url,
            params: params,
            success: function (response) {
                var chart = eval(response.responseText);
                that.getView().add(chart);
            },
            failure: function (response) {
                console.error(response);
            }
        });
    },
    clearChart: function () {
        this.getView().removeAll();
    }
});
