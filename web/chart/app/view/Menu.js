
Ext.define("Chart.view.Menu", {
    extend: "Ext.tree.Panel",
    itemId: "mapPanel33",
    requires: [
        'Chart.view.MenuController',
        'Chart.view.MenuModel',
        'Ext.data.TreeModel'
    ],
    controller: "menu",
    viewModel: {
        type: "menu"
    },
    listeners: {
        select: "onSelectionChanged"
    },
    rootVisible: false
});
