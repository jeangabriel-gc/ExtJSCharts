Ext.define('Chart.view.MenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.menu',
    listen: {
        controller: {
            '*': {
                configLoaded: 'loadMenu'
            }
        }
    },
    loadMenu: function () {

        var store = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true,
                children: [
                    {text: "Premade SQL Statement", expanded: true},
                    {text: "SQL Generated from config", expanded: true},
                    {text: "Other chart examples", expanded: true}
                ]
            }
        });

        // From SQL Statement
        for (var property in Chart.Config.chartFromSqlMap) {
            if (Chart.Config.chartFromSqlMap.hasOwnProperty(property)) {

                store.getRoot().getChildAt(0).appendChild({
                    text: Chart.Config.chartFromSqlMap[property],
                    leaf: true
                });
            }
        }

        // From Tableau-like config
        for (var property in Chart.Config.chartLikeTableauMap) {
            if (Chart.Config.chartLikeTableauMap.hasOwnProperty(property)) {

                store.getRoot().getChildAt(1).appendChild({
                    text: Chart.Config.chartLikeTableauMap[property],
                    leaf: true
                });
            }
        }
        
        // Other examples
        for (var property in Chart.Config.chartOthersMap) {
            if (Chart.Config.chartOthersMap.hasOwnProperty(property)) {

                store.getRoot().getChildAt(2).appendChild({
                    text: Chart.Config.chartOthersMap[property],
                    leaf: true
                });
            }
        }

        this.getView().setStore(store);
    },
    onSelectionChanged: function (menu, node, index) {
        
        if (node.isLeaf()) {

            var parentNode = null;
            var chartName = null;

            if (node.parentNode === this.getView().getStore().getRoot().getChildAt(0)) {
                parentNode = "sql";
                chartName = this.findProperty(node.getData().text, Chart.Config.chartFromSqlMap);
            }
            else if (node.parentNode === this.getView().getStore().getRoot().getChildAt(1)){
                parentNode = "config";
                chartName = this.findProperty(node.getData().text, Chart.Config.chartLikeTableauMap);
            }
            else {
                parentNode = "other";
                chartName = this.findProperty(node.getData().text, Chart.Config.chartOthersMap);
            }
            
            this.fireEvent('selectionChanged', parentNode, chartName);
        }
    },
    findProperty: function (value, obj) {
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {

                if (obj[property] === value) {
                    return property;
                }
            }
        }

        return null;
    }

});
