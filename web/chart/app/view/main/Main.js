/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Chart.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Chart.view.main.MainController',
        'Chart.view.main.MainModel',
        'Chart.view.Menu',
        'Chart.view.Chart'
    ],
    xtype: 'app-main',
    controller: 'main',
    viewModel: {
        type: 'main'
    },
    layout: {
        type: 'border'
    },
    items: [{
            xtype: 'panel',
            bind: {
                title: '{menu}'
            },
            region: 'west',
            //html: '<ul><li>This area is commonly used for navigation, for example, using a "tree" component.</li></ul>',
            width: 250,
            split: true,
            items: [
                    Ext.create('Chart.view.Menu')
            ]
//            items: [{
//                    xtype: 'treepanel',
//                    store: {
//                        root: {
//                            expanded: true,
//                            children: [
//                                {text: "Bar chart", leaf: true},
//                                {text: "Pie chart", leaf: true},
//                                {text: "3D Bar chart", leaf: true},
//                                {text: "MultiLine chart", leaf: true}
//                            ]
//                        }},
//                    listeners: {
//                        select: "testEvent"
//                    },
//                    rootVisible: false
//                }]
//            tbar: [{
//                    text: 'Button',
//                    handler: 'onClickButton'
//                }]
        }, {
            region: 'center',
            xtype: 'panel',
            bind: {
                title: '{mainPanel}'
            },
            items: [
                Ext.create('Chart.view.Chart')
            ]
//            xtype: 'tabpanel',
//            items: [
//                {
//                    title: 'Bar chart',
//                    items: [
//                        Ext.create('Chart.view.charts.BarChart')
//                    ]
//                }, {
//                    title: 'Pie Chart',
//                    items: [
//                        Ext.create('Chart.view.charts.PieChart')
//                    ]
//                },
//                {
//                    title: '3D Bar Chart',
//                    items: [
//                        Ext.create('Chart.view.charts.3DBarChart')
//                    ]
//                },
//                {
//                    title: 'Multi Line Chart',
//                    items: [
//                        Ext.create('Chart.view.charts.MultiLineChart')
//                    ]
//                }]
        }]
});
