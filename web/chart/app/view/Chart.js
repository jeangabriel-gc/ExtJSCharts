
Ext.define("Chart.view.Chart", {
    extend: "Ext.panel.Panel",
    requires: [
        'Chart.view.ChartController',
        'Chart.view.ChartModel',
        'Ext.chart.*'
    ],
    controller: "chart",
    viewModel: {
        type: "chart"
    }
});
